<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'link',
        'title',
        'content',
        'author',
        'date',
    ];

    public function feed()
    {
        return $this->belongsTo('App\Models\Feed');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('read', 'star');
    }
}
