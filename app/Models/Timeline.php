<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    use HasFactory;

    protected $fillable = [
        'link',
        'title',
        'content',
        'author',
        'date',
        'note',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
