<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    use HasFactory;

    protected $fillable = [
        'website',
        'feed',
        'icon',
        'social',
    ];

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('category');
    }
}
